Title: Formatting on Tildes
Slug: formatting
Modified: May 23, 2018
Summary: An overview of Tildes markdown style

[TOC]

Tildes uses a variation of a special formatting language called [Markdown](https://daringfireball.net/projects/markdown/syntax).
If you're familiar with markdown, you should feel right at home. If not, then this doc is for you!

## Special Tildes syntax

First, let's talk about some of the special ways to link to other things on Tildes.

### Linking to groups

If you'd like to link to another group, you can just add a `~` to a word.

For example if you type `~music` it will convert it to [~music](https://tildes.net/~music).

You can also link to subgroups with a `.` after your first word. For example, if you type `~music.metal` it will convert it to [~music.metal](https://tildes.net/~music.metal).

### Linking to other users

If you'd like to link to another user's profile page, you can user the `/u/` or `@` prefix.

For example, typing `@flaque` will convert it to [@flaque](https://tildes.net/user/flaque) and `/u/flaque` will convert to [/u/flaque](https://tildes.net/user/flaque).

### Limited HTML support

At the moment, some HTML elements work. You can create links with:

```
<a href="https://tildes.net">Go to Tildes!</a>
```

Which will create [Go to Tildes!](https://tildes.net)

## Traditional Formatting

The following is a quick cheatsheet for creating headers, bold, italics, and more!

### Headers

You can create big header text with varying `#`.

For example:

```
# h1 - BIGGEST HEADER
## h2 - SMALLER BIGGEST HEADER
### h3 - SMALLER SMALLER BIGGEST HEADER
#### h4 - smaller smaller smaller biggest header
##### h5 - smaller smaller smaller smaller biggest header
###### h6 - smaller smaller smaller smaller smaller biggest header
```

### Bold, Italics, and Strikethroughs.

With one `*asterisk*` you can create _italized_ text.
You can use `**two asterisks**` to create **bold** text.

You can also use `__underlines__` in place of asterisks.

### Lists

You can use `*`, `-` or `+` to create an unordered (bulleted) list.

For example:

```
* dogs
+ cats
- bears
    * Oh my!
```

will create:

* dogs

- cats

* bears
  * Oh my!

You can also use numbers to make an ordered list:

```
1. get the peanut butter
2. get the jelly
3. make lunch
```

Becomes:

1.  get the peanut butter
2.  get the jelly
3.  make lunch

### Links

You can make links with the format: `[text here](https://example.com)`. This will create a link such as [text here](https://example.com).

### Preformatted text and Code

If you'd like to put code inline, you can wrap what you'd like to say with backticks:

```
Checkout my code: `console.log("hello world")` isn't it cool?
```

You can also use three backticks: ``` to put a new block of code:

    ```
    console.log("hello");
    console.log("goodbye");
    ```

### Quotes

You can quote someone with a `>`.

I can quote myself like this:

```
> I can quote myself like this
```

Which renders:

> I can quote myself like this

### Breaks

You can add a break line with `---`.

So if I want to split up some sections, I can do:

```
Chapter 1
Once upon a time

---

Chapter 2
The doggo who knew he was
```

Will render:

Chapter 1
Once upon a time

---

Chapter 2
The doggo who knew he was

# Questions? Comments?

If you've still got a question, feel free to ask it on [~tildes](https://tildes.net/~tildes)!
