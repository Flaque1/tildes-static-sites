# Tildes Blog and Docs

This repo contains the source files used to generate the [Tildes Blog](https://blog.tildes.net) and [Tildes Docs](https://docs.tildes.net) static sites, by using [Pelican](http://docs.getpelican.com/).

## Contributions

In general, contributions (edits or adding new pages) are welcome for the Docs site (files inside the `content/pages/` directory). However, please do not submit edits to these specific pages without prior discussion/approval:

* Code of Conduct (code-of-conduct.md)
* Privacy Policy (privacy-policy.md)
* Terms of Use (terms-of-use.md)

The Blog posts (files in the base `content/` directory) are not open for user contributions, and are mostly included here to make their edit history publicly available.

You may also contribute changes to the theme and templates, but if you do so, keep in mind that the theme is shared across both the Blog and Docs sites and changes may affect both.

## License

In order to support the above:

* Docs pages are licensed under [Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
* Blog posts are not licensed, and are the property of their respective authors.
* Source code used to format the pages (theme, templates, configuration) is licensed under [the MIT license](https://opensource.org/licenses/MIT).

Any contributions will be licensed under the same terms.
